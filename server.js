require('dotenv').config();
const express = require('express'); //referencia al paquete express
const userFile = require('./user.json');
const bodyParser = require('body-parser');
const requestJSON = require('request-json');
const apikeyMLab = 'apiKey=' + process.env.MLABAPIKEY;

var app = express(); // creamos el servidor node
app.use(bodyParser.json());
var port = process.env.PORT || 3000;
//var port = 3000;
const URL_BASE = '/api-peru/v1/';
const mLabURLbase = 'https://api.mlab.com/api/1/databases/techuXXdb/collections/';
const apiKey = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';

// Operación GET todos los usuarios (Collections) (user.json)
app.get (URL_BASE + 'users/',
    function(request, response) {
      response.status(200);
      response.send(userFile);
});

// Operación GET todos los usuarios (Collections) (user.json)
app.get(URL_BASE + 'userstotal',
    function(request, response) {
      console.log(request.query);
      let total = userFile.length;
      console.log(total)
      let totalJson = JSON.stringify({num_elem : total});
      response.status(200).send(totalJson);
      // response.status(200).send({"total": total});
});

// Operación GET a un usuario con ID (Instance)
app.get(URL_BASE + 'users/:id',
    function(request, response) {
      let indice = request.params.id;
      let respuesta =
        (userFile[indice-1] == undefined) ? {"msg":"No existe"} : userFile[indice-1]
      response.status(200).send(respuesta);
});

// Peticion GET con query string
app.get (URL_BASE + 'usersq',
    function(request, response) {
      console.log(request.query);
      let query = request.query;
      console.log(query.max);
      var temp = [];
      var i;
      for (i = 0; i < query.max; i++) {
        temp.push(userFile[i])
        }
      response.status(200);
      response.send(temp);
});

// Operacion GET total
app.get (URL_BASE + 'userstotal',
    function(request, response) {
      console.log(request.query);
      let total = userFile.length;
      let totalJson = JSON.stringify({num_elem : total})
      response.status(200).send({"total": total})
//      response.send(userFile[indice -1]);
});

// operacion POST a users
app.post(URL_BASE + 'users',
  function(req, res) {
      //console.log(reg.body);
      //console.log(reg.body.id);
  let newUser = {
    id: req.body.id,
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email,
    password: req.body.password
  }
  userFile.push(newUser);
  res.status(201);
  res.send({"mensaje":"Usuario creado correctamente",
  "usuario":newUser,
  "userFile actualizado":userFile});
})

// operacion POST a users incremento automatico
app.post(URL_BASE + 'userse',
  function(req, res) {
      //console.log(reg.body);
      //console.log(reg.body.id);
  let total = userFile.length;
  let newUser = {
    id: ++total,
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email,
    password: req.body.password
  }
  console.log(newUser);
  userFile.push(newUser);
  res.status(201);
  res.send({"mensaje":"Usuario creado correctamente",
  "usuario":newUser,
  "userFile actualizado":userFile});
});

// operacion PUT a users
app.put(URL_BASE + 'users/:id',
  function(request, response) {
    let indice = request.params.id;
    console.log(indice)
    console.log(indice-1)
    let firstName = request.body.first_name
    let lastName = request.body.last_name
    let respuesta =
           (userFile[indice-1] == undefined) ? {"msg":"No existe"} : userFile[indice-1]
         response.status(200).send(respuesta);

    var fruits = ["Banana", "Orange", "Apple", "Mango"];
    fruits.splice(2, 0, "Lemon", "Kiwi");
    console.log(fruits)
    let user = userFile[indice-1]
    console.log(user)
    user.splice(2,2,{"firstName": "firstName"},{"lastName": "lastName"});
    console.log(user)
/*
    let respuesta2 =
      (userFile[indice-1] == undefined) ?
      {"msg":"No existe"} :
      "Usuario modificado" : (user.splice(2,2,firstName,lastName));
    response.status(200).send(respuesta2);
*/
});

// operacion DELETE a users
app.delete(URL_BASE + 'users/:id',
  function(request, response) {
  let indice = request.params.id;
  let respuesta =
    (userFile[indice-1] == undefined) ?
    {"msg":"No existe"} :
    {"Usuario eliminado" : (userFile.splice(indice-1,1))};
  response.status(200).send(respuesta);
});

// LOGIN - user.json
 app.post(URL_BASE + 'login',
  function(request, response) {
  // console.log("POST /apitechu/v1/login");
   console.log(request.body.email);
   console.log(request.body.password);
   let user = request.body.email;
   let pass = request.body.password;
   for(us of userFile) {
    if(us.email == user) {
     if(us.password == pass) {
      us.logged = true;
      writeUserDataToFile(userFile);
      console.log("Login correcto!");
      response.send({"msg" : "Login correcto.",
              "idUsuario" : us.id,
               "logged" : "true"});
     } else {
      console.log("Login incorrecto.");
      response.send({"msg" : "Login incorrecto."});
     }
    }
   }
 });
 // LOGOUT - users.json
 app.post(URL_BASE + 'logout',
  function(request, response) {
   //console.log("POST /apitechu/v1/logout");
   console.log(request.body.id)
   var userId = request.body.id;
   for(us of userFile) {
    if(us.id == userId) {
     if(us.logged) {
      delete us.logged; // borramos propiedad 'logged'
      writeUserDataToFile(userFile);
      console.log("Logout correcto!");
      response.send({"msg" : "Logout correcto.", "idUsuario" : us.id});
     } else {
      console.log("Logout incorrecto.");
      response.send({"msg" : "Logout incorrecto."});
     }
    }
   }
 });
 function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./user.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
    if(err) {
     console.log(err);
    } else {
     console.log("Datos escritos en 'user.json'.");
    }
   })
 }

 // Petición GET id con mLab
 app.get(URL_BASE + 'users/:id',
   function (req, res) {
     console.log("GET /colapi/v3/users/:id");
     console.log(req.params.id);
     var id = req.params.id;
     var queryString = 'q={"id":' + id + '}&';
     var queryStrField = 'f={"_id":0}&';
     var httpClient = requestJSON.createClient(mLabURLbase);
     httpClient.get('user?' + queryString + queryStrField + apikeyMLab,
       function(err, respuestaMLab, body){
         console.log("Respuesta mLab correcta.");
       //  var respuesta = body[0];
         var response = {};
         if(err) {
             response = {"msg" : "Error obteniendo usuario."}
             res.status(500);
         } else {
           if(body.length > 0) {
             response = body;
           } else {
             response = {"msg" : "Usuario no encontrado."}
             res.status(404);
           }
         }
         res.send(response);
       });
 });

//servidor escuchara en la url (servidor local)
// http://localhost:3000/holamundo
app.listen(port, function() {
  console.log('Node JS escuchando en el puerto 3000..');
});

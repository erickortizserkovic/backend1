#Imagen inicial base/inicial
FROM node:latest

# Crear directorio de trabajo del contenedor docker
WORKDIR /docker-api-peru

# Copiar archivos del proyecto en directorio del contenedor
ADD . /docker-api-peru

# Instalar dependencias de produccion
# RUN npm install --only=production

# Exponer un puerto de escucha del contenedor (mismo definido en API REST)
EXPOSE 3000

# Lanzar los comandos necesarios para ejecutar la API
#CMD ["node", "server.js"]
CMD ["npm", "run", "pro"]
